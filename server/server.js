const io = require('socket.io')();



io.on('connection', (socket) => {
    // here you can start emitting events to the client

    //chat_msg 이벤트에 대한 처리 리스너..
    socket.on('chat_msg', (msg)=>{
        //클라이언트에 신호를 방출한다(emit)......
        io.emit('chat_msg', msg)
    })
});

const port = 8000;
io.listen(port);
console.log('listening on port ', port);
console.log('listening on port ', port);
console.log('listening on port ', port);
