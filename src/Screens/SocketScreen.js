import * as React from 'react';
import FlexBox from 'flexbox-react';
import CLayout from "../Components/CLayout";
import openSocket from 'socket.io-client';
import {Button, Icon, Input} from "antd";
import Form from "antd/es/form";

const socket = openSocket('http://localhost:7777');
type Props = {
    history: any,

};
type State = {
    results: any,
    message: string,

};

export default class SocketScreen extends React.Component<Props, State> {

    state = {
        results: [],
        message: '',
    }

    constructor(props) {
        super(props)
    }

    componentDidMount(): void {

        socket.on('chat message', (msg) => {
            console.log('message====>' + msg)

            let __results = this.state.results;

            let mergedList = __results.concat(msg);

            this.setState({
                results: mergedList,
            })

        })
    }

    render() {
        return (
            <CLayout history={this.props.history}>

                <Input
                    prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                    placeholder="message"
                    onChange={(e) => {

                        let _value = e.target.value

                        console.log('_value====>' + _value);

                        this.setState({
                            message: _value,
                        })

                    }}
                />
                <Button onClick={() => {

                    socket.emit('chat message', this.state.message)
                }}>
                    emit
                </Button>
                {this.state.results.map(item => {

                    return (
                        <div>
                            {item}
                        </div>
                    )
                })}

            </CLayout>
        );
    };
};
