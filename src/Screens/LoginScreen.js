// @flow
import * as React from 'react';
import {Button, Icon, Input, notification} from "antd";
import FlexBox from 'flexbox-react';
import Form from "antd/es/form";
import {SCREENS} from "../Components/Constants";
import * as Cookie from "js-cookie";

type Props = {
    history: any,
};
type State = {};

export default class LoginScreen extends React.Component<Props, State> {
    state = {
        //auth: true
    }

    constructor(props) {
        super(props)
    }


    componentDidMount() {
        if (!Cookie.get('auth')) {
        } else {
            notification.success({
                duration: 2.0,
                message: ' 로그인 정보가 존재합니다..!! ',
                onClick: () => {
                    console.log('Notification Clicked!');
                },
            });
            this.props.history.push(SCREENS.HomeScreen)
        }
    }


    render() {
        return (
            <div style={{width: window.innerWidth, backgroundColor: 'white', height: window.innerHeight}}>
                <FlexBox style={{justifyContent: 'center', alignItems: 'center', marginTop: 50}}>
                    로그인 스크린
                </FlexBox>
                <FlexBox style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Form layout="inline" style={{}}>
                        <Form.Item>
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                placeholder="Username"
                                value={'guest'}
                            />
                        </Form.Item>
                        <Form.Item>

                            <Input
                                prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                type="user"
                                placeholder="Password"
                                value={'guest'}
                            />
                        </Form.Item>
                    </Form>
                </FlexBox>
                <FlexBox style={{flexDirection: 'row', marginTop: 30, width: window.innerWidth}}>
                    <Button style={{backgroundColor: 'skyblue', width: window.innerWidth / 2}}
                            onClick={() => {
                                /*  const {history} = this.props;
                                  history.entries = [];
                                  history.index = -1;
                                  history.replace(SCREENS.MainScreen);*/
                                //this.props.history.replace(SCREENS.HomeScreen)



                                Cookie.set('auth', 'kyungjoongo');
                                const {history} = this.props;
                                history.entries = [];
                                history.index = -1;
                                history.replace(SCREENS.HomeScreen);
                            }}
                    >
                        로그인

                    </Button>
                    <Button
                        style={{backgroundColor: 'green', width: window.innerWidth / 2}}
                        onClick={() => {

                            this.props.history.push(SCREENS.RegisterScreen)
                        }}
                    >
                        가입
                    </Button>


                </FlexBox>
                <br/>
                <br/>
                <br/>
                <br/>
                <Button
                    style={{backgroundColor: 'red', width: window.innerWidth / 2}}
                    onClick={() => {

                        this.props.history.push(SCREENS.TestScreen003)
                    }}
                >
                    TestSCreen003
                </Button>

            </div>
        );
    };
};
