// @flow
import * as React from 'react';
import {observer} from "mobx-react";
import {Button, Layout} from "antd";
import CLayout from "../Components/CLayout";
import Flexbox from "flexbox-react";
import GlobalStore from "../GlobalStates/GlobalStore";

const {Content, Footer, Sider} = Layout;
type Props = {};
type State = {};


export default observer(
    class TestScreen003 extends React.Component<Props, State> {
        render() {
            return (

                <CLayout history={this.props.history}>

                    <Flexbox>
                        TestScreen003
                    </Flexbox>
                    <div>
                        GlobalStore.counter==> {GlobalStore.counter}
                    </div>
                    <Button style={{backgroundColor: 'maroon', color: 'white'}}>
                        sdfdsf
                    </Button>
                    <Button style={{backgroundColor: 'maroon', color: 'white'}}>
                        sdfdsf
                    </Button>
                    <Button style={{backgroundColor: 'maroon', color: 'white'}}>
                        sdfdsf
                    </Button>
                    <Button style={{backgroundColor: 'maroon', color: 'white'}}
                        onClick={()=>{
                            this.props.history.push('/DetailScreen')
                        }}
                    >
                        onClick
                    </Button>

                </CLayout>

            );
        };
    }
)
