// @flow
import * as React from 'react';
import {observer} from "mobx-react";
import {Button, Layout, notification} from "antd";
import CLayout from "../Components/CLayout";
import GlobalStore from "../GlobalStates/GlobalStore";
import {Tabs} from 'antd';
import {SCREENS} from "../Components/Constants";

const {TabPane} = Tabs;
const {Header, Content, Footer, Sider} = Layout;


type Props = {
    history: any,
};
type State = {
    tabIndex: number,

};

export default observer(
    class TestScreen004 extends React.Component<Props, State> {

        state = {
            tabIndex: 1,
        }

        constructor(props: Props) {
            super(props)
        }


        renderTabBody(index) {

            if (index === 1) {
                return (
                    <div style={{marginLeft: 10,}}>
                        Tab 1asdasdsa<br/>
                        Tab 1asdasdsa<br/>
                        Tab 1asdasdsa<br/>
                        Tab 1asdasdsa<br/>
                        Tab 1asdasdsa<br/>
                        Tab 1asdasdsa<br/>
                        Tab 1asdasdsa<br/>
                        Tab 1asdasdsa<br/>
                        <Button style={{backgroundColor: 'red', color: 'white'}} onClick={() => {

                            this.props.history.push(SCREENS.DetailScreen002)
                        }}>
                            DetailScreen
                        </Button>
                    </div>
                )
            }

            if (index === 2) {
                return (
                    <div style={{marginLeft: 10,}}>
                        Tab 2222<br/>
                        Tab 222222<br/>
                        Tab 22222<br/>
                        Tab 2222<br/>
                        <Button style={{backgroundColor: 'red', color: 'white'}} onClick={() => {
                            this.props.history.push(SCREENS.DetailScreen002)
                        }}>
                            DetailScreen
                        </Button>
                    </div>
                )
            }

            if (index === 3) {
                return (
                    <div style={{marginLeft: 10,}}>
                        Tab 3333333<br/>
                        Tab 3333333<br/>
                        Tab 3333333<br/>
                        Tab 3333333<br/>
                        <Button style={{backgroundColor: 'red', color: 'white'}} onClick={() => {
                            this.props.history.push(SCREENS.DetailScreen002)
                        }}>
                            DetailScreen
                        </Button>
                    </div>
                )
            }
        }


        render() {
            return (
                <CLayout history={this.props.history}>

                    <Tabs defaultActiveKey={GlobalStore.currentTabIndex}
                          size={'medium'}
                          type={'card'}
                          style={{width: window.innerWidth}}
                          onChange={(index) => {
                              GlobalStore.setCurrentTabIndex(index.toString())
                          }}>
                        <TabPane tab="Tab 1" key="1">
                            {this.renderTabBody(1)}
                        </TabPane>
                        <TabPane tab="Tab 2" key="2">
                            <div style={{marginLeft: 10,}}>
                                {this.renderTabBody(2)}
                            </div>
                        </TabPane>
                        <TabPane tab="Tab 3" key="3">
                            <div style={{marginLeft: 10,}}>
                                {this.renderTabBody(3)}
                            </div>
                        </TabPane>
                    </Tabs>
                </CLayout>

            );
        };
    }
)
