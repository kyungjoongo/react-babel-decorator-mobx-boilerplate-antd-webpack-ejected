// @flow
import Flexbox from "flexbox-react";
import * as React from 'react';
import {Observable} from "rxjs-compat";
import axios from 'axios';
import CLayout from "../Components/CLayout";
import {Button, notification} from "antd";
import {observer} from "mobx-react";
import GlobalStore from "../GlobalStates/GlobalStore";

export default observer(
    class StreamExampleScreen extends React.Component {
        state = {
            loading: false,
            results: [],
            observableResult: '',
        }

        constructor(props) {
            super(props)
        }


        componentDidMount(): void {


        }

        requestCreateInstance(delay) {

            GlobalStore.toggleLoading(true)

            let url = 'http://35.221.245.158:8080/TickTock?time=' + delay;

            new Observable(async (observer) => {
                axios({
                    method: 'get',
                    url: url,
                    /*data: {
                        firstName: 'go',
                        lastName: 'kyungjoon'
                    }*/
                }).then((res => {
                    let __data = res.data;
                    console.log('__data__data==============>', __data);
                    observer.next(__data);
                    observer.complete();
                })).catch(err => {
                    observer.error(err)
                })
            }).subscribe({
                next: data => {
                    console.log('got value ' + data)
                    this.setState({
                        observableResult: data + "\n\n\n\n\n..streaming complete!!",
                    })
                },
                error: err => {
                    alert('watch.Error====>' + err)
                },
                //@todo:Listener when the stream request completes.
                complete: () => {
                    console.log('!!!!!!DONE!!!!!!');

                    GlobalStore.toggleLoading(false)

                    notification.success({
                        duration: 2.0,
                        message: delay.toString() + ' TASKS COMPLETED!!!!!!STREAMING DONE!!!!!!\'',
                        onClick: () => {
                            console.log('Notification Clicked!');
                        },
                    });

                },
            })


        }

        render() {
            return (
                <CLayout history={this.props.history}>
                    <div>
                        <div style={{fontSize: 50}}>
                            {this.state.observableResult}
                        </div>
                    </div>

                    <Flexbox syle={{flexDirection: 'column'}}>


                        <div>
                            <Button onClick={() => this.requestCreateInstance(3)} style={{backgroundColor: 'skyblue'}}>
                                call streaming api (TICKTOCK) delay 3second
                            </Button>
                        </div>

                        <br/>
                        <div>
                            <Button onClick={() => this.requestCreateInstance(5)} style={{backgroundColor: 'skyblue'}}>
                                call streaming api (TICKTOCK) delay 5second
                            </Button>
                        </div>
                        <div>
                            <Button onClick={() => this.requestCreateInstance(10)} style={{backgroundColor: 'skyblue'}}>
                                call streaming api (TICKTOCK) delay 10second
                            </Button>
                        </div>


                    </Flexbox>
                    <Button
                        onClick={() => {
                            this.props.history.goBack();
                        }}
                        style={{backgroundColor: 'green', color: 'white'}}>
                        goBack
                    </Button>
                    <Button
                        onClick={() => {
                            this.props.history.goBack();
                        }}
                        style={{backgroundColor: 'blue', color: 'white'}}>
                        goBack
                    </Button>
                    <Button
                        onClick={() => {
                            this.props.history.goBack();
                        }}
                        style={{backgroundColor: 'red', color: 'white'}}>
                        goBack
                    </Button>
                    <Flexbox style={{flexDirection: 'column'}}>
                        <div>
                            <Button
                                onClick={() => {
                                    this.props.history.goBack();
                                }}
                                style={{backgroundColor: 'red', color: 'white'}}>
                                goBack
                            </Button>
                        </div>
                        <div>
                            <Button
                                onClick={() => {
                                    this.props.history.goBack();
                                }}
                                style={{backgroundColor: 'red', color: 'white'}}>
                                goBack
                            </Button>
                        </div>
                        <div>
                            <Button
                                onClick={() => {
                                    this.props.history.goBack();
                                }}
                                style={{backgroundColor: 'red', color: 'white'}}>
                                goBack
                            </Button>
                        </div>
                        <div>
                            <Button
                                onClick={() => {
                                    this.props.history.goBack();
                                }}
                                style={{backgroundColor: 'red', color: 'white'}}>
                                goBack
                            </Button>
                        </div>
                        <div>
                            <Button
                                onClick={() => {
                                    this.props.history.goBack();
                                }}
                                style={{backgroundColor: 'red', color: 'white'}}>
                                goBack
                            </Button>
                        </div>


                    </Flexbox>


                </CLayout>
            );
        };
    }
)
