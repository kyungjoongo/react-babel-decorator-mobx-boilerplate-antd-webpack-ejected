import React, {Component} from 'react';
import {withRouter} from "react-router-dom";
import Flexbox from 'flexbox-react';
import {observer} from "mobx-react";
import GlobalStore from "../GlobalStates/GlobalStore";
import {Button, Layout, Spin} from "antd";
import '../App.css';
import CLayout from "../Components/CLayout";

const {Header, Content, Footer, Sider} = Layout;
type Props = {
    counter: any,
    counter2: number,
    loading: boolean,

}

type State = {
    result: any,
    list: any,
    id: number,
    items: any,
    results: any,
}


export default observer(
    class DetailScreen002 extends Component<Props, State> {
        constructor(props) {
            super(props);
            this.state = {
                loading: false,
                id: 0,
                items: {},
                results: [1, 2, 3, 4,]
            }
        }

        componentDidMount(): void {

        }


        render() {
            return (
                <CLayout history={this.props.history}>
                    <Flexbox style={{flexDirection: 'column', textAlign: 'center'}}>
                        <div>
                            name : {this.state.items.name}
                        </div>
                        <br/>
                        <div>
                            age : {this.state.items.age}
                        </div>
                        <div>
                            counter : {GlobalStore.counter}
                        </div>
                    </Flexbox>
                    <Flexbox>


                        <br/>
                        <Flexbox style={{justifyContent: 'center', alignItems: 'center'}}>
                            {GlobalStore.loading && <Spin/>}
                        </Flexbox>

                        <br/>
                        <Flexbox style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 20,
                            flexDirection: 'column'
                        }}>
                            <Button
                                onClick={() => {
                                    this.props.history.goBack();
                                }}
                                style={{backgroundColor: 'green', color: 'white'}}>
                                goBack
                            </Button>
                            <Button
                                onClick={() => {
                                    this.props.history.goBack();
                                }}
                                style={{backgroundColor: 'pink', color: 'white'}}>
                                goBack
                            </Button>
                            <Button
                                onClick={() => {
                                    this.props.history.goBack();
                                }}
                                style={{backgroundColor: 'grey', color: 'white'}}>
                                goBack
                            </Button>
                            <Button
                                onClick={() => {
                                    this.props.history.goBack();
                                }}
                                style={{backgroundColor: 'grey', color: 'white'}}>
                                goBack
                            </Button>
                            <div>
                                고경준 천재님이십니디sdlkflsdkflsdkf
                            </div>
                        </Flexbox>
                    </Flexbox>
                </CLayout>


            );
        }
    }
)
