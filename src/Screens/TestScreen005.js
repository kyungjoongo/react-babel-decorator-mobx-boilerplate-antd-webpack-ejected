// @flow
import * as React from 'react';
import {observer} from "mobx-react";
import {Button, Layout, notification} from "antd";
import CLayout from "../Components/CLayout";
import Flexbox from "flexbox-react";
const {Header, Content, Footer, Sider} = Layout;
type Props = {};
type State = {};

export default observer(
    class TestScreen005 extends React.Component<Props, State> {
        render() {
            return (
                <CLayout history={this.props.history}>


                    TestScreen005

                    <Flexbox>
                        <Button style={{backgroundColor: 'grey', color: 'white'}}>
                            sdfdsf
                        </Button>
                    </Flexbox>
                    <Flexbox style={{height:5}}/>
                    <Flexbox>
                        <Button style={{backgroundColor: 'blue', color: 'white'}}>
                            sdfdsf
                        </Button>
                    </Flexbox>
                    <Flexbox style={{height:5}}/>
                    <Flexbox>
                        <Button style={{backgroundColor: 'red', color: 'white'}}>
                            sdfdsf
                        </Button>
                    </Flexbox>
                    <Flexbox style={{height:5}}/>
                    <Flexbox>
                        <Button onClick={()=>{

                            notification.success({
                                duration: 2.0,
                                message: ' Notification!!!!!! ',
                                onClick: () => {
                                    console.log('Notification Clicked!');
                                },
                            });
                        }} style={{backgroundColor: 'green', color: 'white'}}>
                            1Notification
                        </Button>
                        <Button style={{backgroundColor: 'green', color: 'white'}}>
                            2
                        </Button>
                        <Button style={{backgroundColor: 'green', color: 'white'}}>
                            3
                        </Button>
                        <Button style={{backgroundColor: 'green', color: 'white'}}>
                            4
                        </Button>
                    </Flexbox>
                </CLayout>

            );
        };
    }
)
