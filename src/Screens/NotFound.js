import React from 'react';
import {Link} from 'react-router-dom';

const NotFound = () => (
    <div>
        <div style={{textAlign: 'center', fontSize: 25}}>
            Page NOT FOUND!!!!!!!!!!!!
        </div>
        <center><Link to="/">Return to Home Page</Link></center>
    </div>
);
export default NotFound;
