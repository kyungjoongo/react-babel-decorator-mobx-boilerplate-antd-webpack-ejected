import React, {Component} from 'react';
import {withRouter} from "react-router-dom";
import Flexbox from 'flexbox-react';
import {observer} from "mobx-react";
import GlobalStore from "../GlobalStates/GlobalStore";
import {Button, Layout, Spin} from "antd";
import '../App.css';
import CLayout from "../Components/CLayout";

const {Header, Content, Footer, Sider} = Layout;
type Props = {
    counter: any,
    counter2: number,
    loading: boolean,

}

type State = {
    result: any,
    list: any,
    id: number,
    items: any,
    results: any,
}


export default observer(
    class DetailScreen extends Component<Props, State> {
        constructor(props) {
            super(props);
            this.state = {
                loading: false,
                id: 0,
                items: {},
                results: [1, 2, 3, 4,]
            }
        }

        componentDidMount(): void {
            //@todo : 이전화면에서 넘어온 라우터 파라매터 value
          /*  let items = this.props.location.state.items;
            this.setState({
                items
            })

            console.log('items===>', items);*/

        }


        render() {
            return (

                <CLayout history={this.props.history}>
                    <Flexbox style={{flexDirection: 'column', textAlign: 'center'}}>
                        <div>
                            name : {this.state.items.name}
                        </div>
                        <br/>
                        <div>
                            age : {this.state.items.age}
                        </div>
                        <div>
                            counter : {GlobalStore.counter}
                        </div>
                    </Flexbox>
                    <Flexbox>



                        <div>
                            디테일 스크린
                        </div>
                    </Flexbox>
                </CLayout>


            );
        }
    }
)
