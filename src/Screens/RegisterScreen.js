// @flow
import * as React from 'react';
import {Button, Icon, Input, notification} from "antd";
import FlexBox from 'flexbox-react';
import Form from "antd/es/form";
import {SCREENS} from "../Components/Constants";
import * as Cookie from "js-cookie";

type Props = {
    history: any,

};
type State = {};

export default class RegisterScreen extends React.Component<Props, State> {


    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div style={{width: window.innerWidth, backgroundColor: 'white', height: window.innerHeight}}>
                <FlexBox style={{justifyContent: 'center', alignItems: 'center', marginTop: 50}}>
                    가입 스크린
                </FlexBox>
                <FlexBox style={{alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
                    <Form layout="inline" style={{}}>
                        <Form.Item>
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                placeholder="name"
                            />
                        </Form.Item>
                        <br/>
                        <Form.Item>
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                placeholder="id"
                            />
                        </Form.Item>
                        <br/>
                        <Form.Item>
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                placeholder="password"
                            />
                        </Form.Item>
                        <br/>
                        <Form.Item>
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                placeholder="sex"
                            />
                        </Form.Item>
                        <br/>
                        <Form.Item>
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                placeholder="age"
                            />
                        </Form.Item>
                        <br/>
                    </Form>
                </FlexBox>
                <FlexBox style={{flexDirection: 'row', marginTop: 30, width: window.innerWidth}}>

                    <Button
                        style={{backgroundColor: 'green', width: window.innerWidth / 2}}
                        onClick={() => {
                            this.props.history.goBack();
                        }}
                    >
                        goBack
                    </Button>

                    <Button
                        style={{backgroundColor: 'red', width: window.innerWidth / 2}}
                        onClick={() => {

                            notification.success({
                                duration: 2.0,
                                message: ' 등록완료!! ',
                                onClick: () => {
                                    console.log('Notification Clicked!');
                                },
                            });

                        }}
                    >
                        Register
                    </Button>

                </FlexBox>

            </div>
        );
    };
};
