import React, {Component,} from 'react';
import {Button, Layout, Spin} from 'antd';
import axios from 'axios';
import {observer} from "mobx-react";
import '../App.css';
import Flexbox from "flexbox-react";
import GlobalStore from "../GlobalStates/GlobalStore";
import CircularProgress from "@material-ui/core/CircularProgress";
import CLayout from "../Components/CLayout";

const {Content, Footer, Sider} = Layout;
type Props = {
    counter: any,
    counter2: number,
    loading: boolean,
}

type State = {
    results: any,
    list: any,
    loading: boolean,
    currentPage: number,
    totalCount: number
}


export default observer(
    class MainScreen extends Component<Props, State> {

        constructor(props: Props) {
            super(props);
            this.state = {
                results: [],
                loading: false,
                currentPage: 1,
                totalCount: 0,
            }

        }

        componentDidMount() {
            this.getLength();
            this.getList();
        }

        getLength() {
            axios.get('http://35.221.245.158:8080/image?offset=0&max=10000').then((response) => {
                let totalCount = response.data.length;

                this.setState({
                    totalCount: totalCount,
                })
            });
        }

        getList() {
            this.setState({
                loading: true,
            })
            let offset = (this.state.currentPage - 1) * 10;
            axios.get('http://35.221.245.158:8080/image?offset=' + offset + '&max=10').then((response) => {
                console.log(response.data);
                this.setState({
                    results: response.data,

                }, () => {
                    setTimeout(() => {
                        this.setState({
                            loading: false,
                        })
                    }, 1500)
                })


            });
        }

        render() {
            return (

                <CLayout history={this.props.history}>
                    <Flexbox>
                        {this.state.loading &&
                        <div  style={{justifyContent: 'center', alignItems: 'center', position: 'absolute', left: '50%'}}>
                            <Spin/>
                        </div>
                        }

                        <Flexbox
                            style={{justifyContent: 'center', alignItems: 'center', position: 'absolute', left: '50%'}}>
                            {GlobalStore.loading && <Spin/>}
                        </Flexbox>
                        <Flexbox style={{justifyContent: 'center', alignItems: 'center'}}>
                            <Button onClick={() => {
                                GlobalStore.toggleLoading()
                            }}>
                                toggleLoading
                            </Button>
                        </Flexbox>
                        <div style={{marginTop: 25,}}>


                            <Flexbox
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    fontSize: 20,
                                    color: 'black'
                                }}>
                                Counter : {GlobalStore.counter}
                            </Flexbox>
                            <Flexbox
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    fontSize: 20,
                                    color: 'black'
                                }}>
                                Counter002 : {GlobalStore.counter002}
                            </Flexbox>
                            <Flexbox>
                                {GlobalStore.loading002 && <CircularProgress/>}
                            </Flexbox>
                            <Flexbox style={{justifyContent: 'center', alignItems: 'center', marginTop: 15,}}>
                                <Button
                                    style={{backgroundColor: 'blue', color: 'white'}}
                                    onClick={() => {

                                        GlobalStore.incrementCount()
                                    }}>
                                    incrementCount
                                </Button>
                                <Flexbox style={{width: 30,}}/>
                                <Button
                                    style={{backgroundColor: 'red', color: 'white'}}
                                    onClick={() => {

                                        GlobalStore.decrementCount()
                                    }}>
                                    decrementCount
                                </Button>
                                <Flexbox style={{width: 30,}}/>
                                <Button
                                    onClick={() => {

                                        GlobalStore.setHeaderTitle('Main(DetailSCreen)')
                                        this.props.history.push('/DetailScreen', {
                                            id: 9,
                                            items: {
                                                name: 'kyungjoon',
                                                age: 41,
                                                sex: 'male',
                                                brain: 'genius',
                                            }
                                        })
                                    }}
                                >
                                    push detailscreen
                                </Button>
                            </Flexbox>
                            <Flexbox style={{flexDirection: 'column', marginTop: 50}}>
                                <Flexbox
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        flexDirection: 'column'
                                    }}>
                                    {this.state.results.map((item: any, index: number) => {
                                        return (
                                            <div key={index}>
                                                <img
                                                    src={`http://35.221.245.158:8080/upload/getImage?filename=${item.name}`}
                                                    style={{width: 150, height: 150,}}/>
                                            </div>
                                        )
                                    })}
                                </Flexbox>
                            </Flexbox>
                        </div>
                    </Flexbox>
                </CLayout>


            );
        }
    }
)
