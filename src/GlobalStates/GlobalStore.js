import {observable} from 'mobx';


class GlobalStore {


    @observable isShowMenu = 'none';
    @observable counter = 0;
    @observable counter002: number = 0;
    @observable loading = false;
    @observable loading002: boolean = false;
    @observable menuIndex = 0;
    @observable headerTitle: string = 'HomeScreen';


    @observable currentTabIndex: string = "1";


    setCurrentTabIndex(index: string) {

        this.currentTabIndex = index;
    }


    setIsShowMenu(value) {
        this.isShowMenu = value;
    }

    incrementCount() {
        this.counter = this.counter + 1;
    }

    decrementCount() {
        this.counter--;
        console.log("decrement", this.counter);
    }

    incrementDouble() {
        this.counter = this.counter + 2;
        console.log("increment2", this.counter);
    }

    toggleLoading(value: boolean = false) {
        this.loading = !this.loading
    }

    toggleLoading002() {
        this.loading002 = !this.loading002
    }

    setMenuIndex(index) {
        this.menuIndex = index;
    }

    setHeaderTitle(value: string) {
        this.headerTitle = value;
    }

}


export default new GlobalStore();
