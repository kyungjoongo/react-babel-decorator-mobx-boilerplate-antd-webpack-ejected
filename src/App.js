import 'react-hot-loader'
import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {hot} from "react-hot-loader/root";
import DetailScreen from "./Screens/DetailScreen";
import LoginScreen from "./Screens/LoginScreen";
import LawyerScreen from "./Screens/LawyerScreen";
import TestScreen004 from "./Screens/TestScreen004";
import TestScreen005 from "./Screens/TestScreen005";
import StreamExampleScreen from "./Screens/StreamExampleScreen";
import SettingScreen from "./Screens/SettingScreen";
import HomeScreen from "./Screens/HomeScreen";
import * as Cookie from "js-cookie";
import requireAuth from "./Components/AuthenticatedComponent";
import RegisterScreen from "./Screens/RegisterScreen";
import './App.css'
import DetailScreen002 from "./Screens/DetailScreen002";
import LawyerDetailScreen from "./Screens/LawyerDetailScreen";
import {AnimatedSwitch} from 'react-router-transition';
import {spring, AnimatedRoute} from 'react-router-transition';





type Props = {
    history: any,
};
type State = {};
export default hot(
    class App extends React.Component<Props, State> {

        constructor(props) {
            super(props)
        }

        state = {
            auth: true
        }

        componentDidMount() {
            if (!Cookie.get('auth')) {
                this.setState({auth: false});
            }
        }

        mapStyles(styles) {
            return {
                opacity: styles.opacity,
                transform: `scale(${styles.scale})`,
            };
        }

// wrap the `spring` helper to use a bouncy config
        bounce(val) {
            return spring(val, {
                stiffness: 330,
                damping: 22,
            });
        }

// child matches will...
        bounceTransition = {
            // start in a transparent, upscaled state
            atEnter: {
                opacity: 0,
                scale: 1.2,
            },
            // leave in a transparent, downscaled state
            atLeave: {
                opacity: this.bounce(0),
                scale: this.bounce(0.8),
            },
            // and rest at an opaque, normally-scaled state
            atActive: {
                opacity: this.bounce(1),
                scale: this.bounce(1),
            },
        };


        render() {

            return (

                <BrowserRouter>
                    <AnimatedSwitch
                        atEnter={this.bounceTransition.atEnter}
                        atLeave={this.bounceTransition.atLeave}
                        atActive={this.bounceTransition.atActive}
                        mapStyles={this.mapStyles}
                        className="route-wrapper"
                    >
                        <Route exact path="/" component={LawyerScreen}/>

                        <Route exact path="/LawyerDetailScreen" component={LawyerDetailScreen}/>
                        <Route exact path="/LoginScreen" component={LoginScreen}/>
                        <Route exact path="/HomeScreen" component={requireAuth(HomeScreen)}/>
                        <Route path="/DetailScreen" component={DetailScreen}/>
                        <Route path="/DetailScreen002" component={DetailScreen002}/>
                        <Route path="/TestScreen004" component={TestScreen004}/>
                        <Route path="/TestScreen005" component={TestScreen005}/>
                        <Route path="/StreamExampleScreen" component={StreamExampleScreen}/>
                        <Route path="/SettingScreen" component={SettingScreen}/>
                        <Route path="/RegisterScreen" component={RegisterScreen}/>
                        {/*<Route path="*" component={NotFound}/>*/}
                    </AnimatedSwitch>

                </BrowserRouter>


            );
        };
    }
)

function About() {
    return <h2>About</h2>;
}

function Users() {
    return <h2>Users</h2>;
}

