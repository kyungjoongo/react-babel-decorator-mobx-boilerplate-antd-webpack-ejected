const proxy = require('http-proxy-middleware');
module.exports = function(app) {
    app.use(proxy('/TickTock',
        {
            target:'http://35.221.245.158:8080/',
            changeOrigin:true
        }
    ));
}
