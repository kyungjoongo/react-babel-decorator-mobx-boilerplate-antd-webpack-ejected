import React from 'react';
import {withRouter} from 'react-router';
import * as Cookie from "js-cookie";
import {notification} from "antd";

export default function requireAuth(Component) {
    class AuthenticatedComponent extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                auth: Cookie.get('auth')
            }
        }

        componentDidMount() {
            this.checkAuth();
        }

        checkAuth() {
            const location = this.props.location;
            const redirect = location.pathname + location.search;
            if (!Cookie.get('auth')) {
                notification.warning({
                    duration: 2.0,
                    message: ' 로그인을정보가 없습니다....',
                    onClick: () => {
                        console.log('Notification Clicked!');
                    },
                });
                this.props.history.replace(`/LoginScreen`);
            }
        }

        render() {
            return Cookie.get('auth') ? <Component {...this.props} /> : null;
        }
    }

    return withRouter(AuthenticatedComponent)
}
