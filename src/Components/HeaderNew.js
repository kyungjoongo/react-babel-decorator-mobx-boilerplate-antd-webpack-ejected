import React, {useEffect} from 'react';
import styled from 'styled-components';
import oc from 'open-color';
import {observer} from "mobx-react";
import GlobalStore from "../GlobalStates/GlobalStore";
import {CircularProgress} from "@material-ui/core";
import {Col, Icon, notification, Row} from "antd";
import Flexbox from "flexbox-react";
import {SCREENS} from "./Constants";
import Loader from 'react-loader-spinner'
import {Layout} from 'antd';
import * as Cookie from "js-cookie";
import Lottie from 'react-lottie';
import animationData from './10522-loading.json'

const {Header, Footer, Sider, Content} = Layout;


const Wrapper = styled.div`
    
    /* 레이아웃 */
    display: flex;
    position: fixed;
    align-items: center;
    justify-content: center;
    height: 60px;
    width: 100%;
    top: 0px;
    z-index: 5;
    font-family: 'Baloo';
    
    
    /* 색상 */
    background-color: #0f336d;
    color: white;
    border-bottom: 1px solid ${oc.indigo[7]};
    box-shadow: 0 3px 6px rgba(0,0,0,0.10), 0 3px 6px rgba(0,0,0,0.20);

    
    /* 폰트 */
    font-size: 2.5rem;
    
    
`;


type Props = {};
type State = {};

export default observer(
    class HeaderNew extends React.Component<Props, State> {
        render() {

            return (
                <Wrapper>
                    <Header style={{width: '100%',}}>
                        <Row>
                            <Col span={14}>{GlobalStore.headerTitle}</Col>
                            <Col span={6} style={{}}>
                                {GlobalStore.loading &&
                                <Flexbox
                                    style={{
                                        justifyContent: 'center',
                                        alignSelf: 'center',
                                        alignItem: 'center',
                                        marginTop: 7,
                                        width: 150,
                                    }}>
                                    {/* <CircularProgress style={{color: 'white'}}/>*/}
                                    <Lottie
                                        options={{
                                            loop: true,
                                            autoplay: true,
                                            animationData: animationData,
                                        }}
                                        height={50}
                                        width={50}
                                        isStopped={false}
                                        isPaused={false}/>
                                </Flexbox>
                                }
                            </Col>
                            <Col span={2}>
                                <Icon type="logout" style={{color: 'white'}} onClick={() => {

                                    Cookie.remove('auth');

                                    notification.info({
                                        duration: 2.0,
                                        message: ' 로그아웃 되었씁니다......',
                                        onClick: () => {
                                            console.log('Notification Clicked!');
                                        },
                                    });


                                    const {history} = this.props;
                                    history.entries = [];
                                    history.index = -1;
                                    history.replace(SCREENS.LoginScreen);
                                }}
                                />
                            </Col>
                            <Col span={2}>
                                <Icon type="setting" style={{color: 'white'}} onClick={() => {
                                    this.props.history.push(SCREENS.SettingScreen)
                                }}
                                />
                            </Col>
                        </Row>
                    </Header>
                </Wrapper>
            );
        };
    }
)

