import StreamExampleScreen from "../Screens/StreamExampleScreen";

export const SCREENS = {

    MainScreen: "MainScreen",
    HomeScreen: "HomeScreen",
    RegisterScreen: "RegisterScreen",
    MainScreen002: "MainScreen002",
    DetailScreen: "DetailScreen",
    DetailScreen002: 'DetailScreen002',
    LoginScreen: "LoginScreen",
    SettingScreen: "SettingScreen",
    TestScreen003: "TestScreen003",
    TestScreen004: "TestScreen004",
    TestScreen005: "TestScreen005",
    StreamExampleScreen: "StreamExampleScreen",


}
