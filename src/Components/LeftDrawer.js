import React from "react";
import {Icon, Layout, Menu} from "antd";
import {observer} from "mobx-react";
import GlobalStore from "../GlobalStates/GlobalStore";
import {SCREENS} from "./Constants";
import styled from "styled-components";
import Flexbox from "flexbox-react";

const {Header, Content, Footer, Sider} = Layout;
const {SubMenu} = Menu;


const StyledWrapper = styled.div`

    color:red;
    
`;

export default observer(
    class LeftDrawer extends React.Component {
        constructor(props) {
            super(props)
        }

        state = {
            current: 'mail',

        };

        handleClick = e => {
            console.log('click ', e);
            this.setState({
                current: e.key,
            });
        };

        render() {
            return (
                <StyledWrapper>
                    <Flexbox style={{height: window.innerHeight}}>
                        <Menu theme="dark" mode="vertical-left">
                            <Menu.Item
                                key="1"
                                onClick={() => {
                                    GlobalStore.setHeaderTitle(SCREENS.HomeScreen)
                                    this.props.history.replace(SCREENS.HomeScreen)

                                }}
                            >
                                <Icon type="menu" style={{color: 'white'}}/>
                                <span style={{}}>HomeScreen</span>
                            </Menu.Item>
                            <Menu.Item
                                key="2"
                                onClick={() => {
                                    GlobalStore.setHeaderTitle(SCREENS.TestScreen003)
                                    const {history} = this.props;
                                    history.entries = [];
                                    history.index = -1;
                                    history.replace(SCREENS.TestScreen003);

                                }}
                            >
                                <Icon type="smile" style={{}}/>
                                <span>TestScreen003</span>
                            </Menu.Item>
                            <Menu.Item key="3" onClick={() => {
                                GlobalStore.setHeaderTitle(SCREENS.TestScreen004)
                                this.props.history.replace(SCREENS.TestScreen004)

                            }}>
                                <Icon type="star"/>
                                <span>TestScreen004</span>
                            </Menu.Item>
                            <Menu.Item key="4" onClick={() => {
                                GlobalStore.setHeaderTitle(SCREENS.TestScreen005)
                                this.props.history.replace(SCREENS.TestScreen005)

                            }}>
                                <Icon type="instagram"/>
                                <span>TestScreen005</span>
                            </Menu.Item>

                            <Menu.Item key="5" onClick={() => {
                                GlobalStore.setHeaderTitle(SCREENS.StreamExampleScreen)
                                this.props.history.replace(SCREENS.StreamExampleScreen)

                            }}>
                                <Icon type="facebook"/>
                                <span>StreamExampleScreen</span>
                            </Menu.Item>

                            <Menu.Item key="6" onClick={() => {

                            }}>
                                <Icon type="facebook"/>
                                <span>SocketScreen</span>
                            </Menu.Item>
                        </Menu>
                    </Flexbox>
                </StyledWrapper>

            )
        }
    }
)
