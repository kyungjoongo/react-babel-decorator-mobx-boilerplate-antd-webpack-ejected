import React from 'react';
import styled from 'styled-components';
import HeaderNew from "./HeaderNew";
import LeftDrawer from "./LeftDrawer";
import Flexbox from "flexbox-react";

const StyledWrapper = styled.div`


    padding-top: 60px; /* 헤더 높이 */
    color: black;
    font-size: 20pt;
    z-index: 99999;
    
`;

type Props = {};
type State = {};

export default class CLayout extends React.Component<Props, State> {

    render() {
        return (
            <StyledWrapper>
                <HeaderNew history={this.props.history}/>

                <Flexbox>
                    <div>

                        <LeftDrawer history={this.props.history}/>
                    </div>
                    <div>
                        {this.props.children}
                    </div>

                </Flexbox>


            </StyledWrapper>
        );
    };
};
