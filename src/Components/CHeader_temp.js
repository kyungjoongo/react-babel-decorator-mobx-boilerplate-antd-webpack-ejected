import React, {useEffect, useState} from 'react';
import Flexbox from "flexbox-react";

export default function CHeader_temp(props) {
    // Declare a new state variable, which we'll call "count"
    const [title, setTitle] = useState('');


    useEffect(() => {
        return () => {

        };
    }, []);

    return (
        <Flexbox style={{
            backgroundColor: 'blue',
            height: 35,
            color: 'white',
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            fontWeight: 'bold'
        }}>
            {props.title}
        </Flexbox>
    );
}
