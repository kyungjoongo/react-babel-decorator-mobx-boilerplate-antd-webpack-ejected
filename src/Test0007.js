// @flow
import * as React from 'react';
import {CircularProgress} from "@material-ui/core";
import * as Rx from "rxjs-compat"; //"rxjs-compat": "^6.5.3",
import axios from 'axios';

export default class Test0007 extends React.Component {
    state = {
        loading: false,
        results: [],
        observerResults: '',
    }

    constructor(props) {
        super(props)
    }

    componentDidMount(): void {
        this.requestCreateInstance();
    }

    requestCreateInstance() {
        this.setState({
            loading: true,
        })

        /*  await setTimeout(() => {
                observer.next(1);
            }, 2500)
            await setTimeout(() => {
                observer.next(2);
            }, 5000)
            await setTimeout(() => {
                observer.next(3);
            }, 7000)
            await setTimeout(() => {
                observer.next(4);
                observer.complete();
            }, 9000);*/
        //let url = 'https://mc-stage.mobiledgex.net:9900/api/v1/auth/ctrl/CreateAppInst';
        let myObservable = Rx.Observable.create(async (observer) => {
            axios({
                url: 'https://mc-dev.mobiledgex.net:9900/api/v1/auth/ctrl/CreateAppInst',
                method: 'post',
                headers: {
                    'Authorization': `Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NzYzMTMyODUsImlhdCI6MTU3NjIyNjg4NSwidXNlcm5hbWUiOiJtZXhhZG1pbiIsImVtYWlsIjoibWV4YWRtaW5AbW9iaWxlZGdleC5uZXQiLCJraWQiOjJ9.o4TLYae57cTTKdarP4TdJK74fdcGcowItyPyRCm3Nrgtu4U9jS-NnC5whQw9JaFIE-m-iA0NkuxoUDKdFN097A
`
                },
                responseType: 'stream',

                data: {
                    "region": "US",
                    "appinst": {
                        "key": {
                            "app_key": {
                                "developer_key": {
                                    "name": "MobiledgeX"
                                },
                                "name": "Face Detection Demo",
                                "version": "1.0"
                            },
                            "cluster_inst_key": {
                                "cluster_key": {

                                },
                                "cloudlet_key": {
                                    "operator_key": {
                                        "name": "MEX"
                                    },
                                    "name": "jlm-dind"
                                }
                            }
                        }
                    }
                }
            }).then((res => {
                let __data = res.data;
                console.log('__data__data==============>', __data);
                observer.next(__data);
                observer.complete();
            })).catch(err => {

                alert(err)
                observer.error(err)
            })
        });




        myObservable.subscribe({
            next: x => {
                console.log('got value ' + x)
                this.setState({
                    observerResults: x,
                })
            },
            error: err => console.error('something wrong occurred: ' + err),
            //@todo:stream 리퀘스트 작업이 완료된경우......
            complete: () => {
                console.log('!!!DONE!!!!!!');
                this.setState({
                    loading: false,
                })
            },
        });
        console.log('just after subscribe');
    }

    render() {
        return (
            <div>
                {this.state.loading && <CircularProgress/>}
                <div style={{fontSize: 50}}>
                    {this.state.observerResults}
                </div>
            </div>
        );
    };
}