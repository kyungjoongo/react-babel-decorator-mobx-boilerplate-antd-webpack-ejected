// @flow
import * as React from 'react';
import {Button, CircularProgress} from "@material-ui/core";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/dom/ajax';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import Flexbox from 'flexbox-react';
import {FetchComponent} from "./FetchComponent";

export default class RxScreen002 extends React.Component {
    state = {
        loading: false,
        results: [],
    }

    constructor(props) {
        super(props)
    }


    componentDidMount(): void {

        this.setState({
            loading: true,
        })
        let url = 'http://kyungjoon77.ipdisk.co.kr:8080/TickTock';

        const data$ = Observable.create(observer => {
            fetch(url)
                .then(response => response.text()) // or text() or blob() etc.
                .then(data => {
                    observer.next(data);
                    observer.complete();
                })
                .catch(err => observer.error(err))
                .finally(() => {
                    console.log('complete!!!');
                    this.setState({
                        loading: false,
                    })
                })
        });


        data$.subscribe(data => {
            console.log('itemeLength===>', data);
            //기조의 데이터를 가지고온다.
            let _results = this.state.results;
            _results.push(data);

            this.setState({
                results: _results,
            })
        });


    }


    render() {
        return (
            <div style={{width: window.innerWidth, backgroundColor: 'grey', height:window.innerHeight}}>
                sdlfksdlkflsdkfl

                <div>
                    sdlkfslkflksdlfksd
                </div>
                <div>
                    sdlkfslkflksdlfksd
                </div>
                {this.state.loading && <CircularProgress/>}
                <Button

                    style={{backgroundColor: 'red', color: 'white'}}
                    onClick={() => {
                        alert('sdfk;lsldkfl')
                    }}
                >
                    lsdklksdfl
                </Button>


                <Flexbox style={{flexDirection: 'column'}}>
                    {this.state.results.map((item, index) => {
                        return (
                            <Flexbox key={index}>
                                {item}
                            </Flexbox>
                        )
                    })}
                </Flexbox>

            </div>
        );
    };
}
