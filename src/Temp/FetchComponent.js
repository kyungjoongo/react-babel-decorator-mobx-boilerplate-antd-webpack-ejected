import * as React from 'react';
import {withPropsStream} from 'react-props-stream'
import {distinctUntilChanged, map, switchMap} from 'rxjs/operators'

export const FetchComponent = withPropsStream(props$ => props$.pipe(map(props => props.url),
    distinctUntilChanged(),
    switchMap(url => {
        fetch(url).then(response => response.text())
    }),
    map(responseText => ({responseText}))
    ),
    props => {
        return (
            <div>The result was: {props.responseText}</div>
        )
    }
)
