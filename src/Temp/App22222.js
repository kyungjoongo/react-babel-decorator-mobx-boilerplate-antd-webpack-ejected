import 'react-hot-loader'
import React from "react";
import {BrowserRouter as Router, Link, Route} from "react-router-dom";
import {hot} from "react-hot-loader/root";
import DetailScreen from "../Screens/DetailScreen";
import SocketIOScreen001 from "./SocketIOScreen001";
import HomeScreen from "../Screens/MainScreen";
import RXScreen002 from "./RXScreen002";
import FlexBox from 'flexbox-react';
import StreamExampleScreen from "../Screens/StreamExampleScreen";

export default hot(
    function App22222222() {
        return (
            <Router>
                <FlexBox>
                    <div style={{marginRight: 25, marginTop: 25,}}>
                        <ul>
                            <li>
                                <Link to="/HomeScreen">Home</Link>
                            </li>
                            <li>
                                <Link to="/Test007">Test007</Link>
                            </li>
                            <li>
                                <Link to="/About">About</Link>
                            </li>
                        </ul>
                    </div>
                    <div style={{backgroundColor: 'black'}}>
                        <Route exact path="/" component={HomeScreen}/>
                        <Route path="/HomeScreen" component={HomeScreen}/>
                        <Route path="/RXScreen002" component={RXScreen002}/>
                        <Route path="/About" component={About}/>
                        <Route path="/Users" component={Users}/>
                        <Route path="/Test007" component={StreamExampleScreen}/>
                        <Route path="/DetailScreen" component={DetailScreen}/>
                        <Route path="/SocketIOScreen001" component={SocketIOScreen001}/>
                    </div>

                </FlexBox>

            </Router>


        );
    }
)

