import * as React from 'react';

export const UseFetchNotes = query => {
    const [notes, setNotes] = React.useState([])

    React.useEffect(() => {
        fetch('api/notes?query=${query}')
            .then(response => response.json())
            .then(data => {
                setNotes(data)
            })
    }, [query])

    return { notes }
}
